package com.example.notepadkotlin

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import com.example.notepadkotlin.adapter.NoteAdapter
import com.example.notepadkotlin.database.DatabaseHepler
import com.example.notepadkotlin.model.NoteDetail
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var noteAdapter: NoteAdapter? = null
    private var listNotes: ArrayList<NoteDetail>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getListOfNote()
        if (listNotes!!.isNotEmpty()) {
            rv_notes.visibility = View.VISIBLE
            noteAdapter = NoteAdapter(listNotes!!)
            rv_notes.adapter = noteAdapter
            tv_plus_instruction.visibility = View.INVISIBLE
        } else {
            rv_notes.visibility = View.INVISIBLE
            tv_plus_instruction.visibility = View.VISIBLE
        }

        btn_add_note.setOnClickListener(View.OnClickListener { v ->
            val intent = Intent(this, AddEditNoteActivity::class.java)
            startActivityForResult(intent, 0X0100)
        })
    }

    private fun getListOfNote() {
        listNotes = DatabaseHepler(this).getAllNotes()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 0X0100) {
            if (resultCode == Activity.RESULT_OK) {
                val note = data!!.getParcelableExtra<NoteDetail>("Task")
                val isModified = data!!.getBooleanExtra("Modified",false)
                if(isModified){
                    noteAdapter!!.notifyItemChanged(1)
                }
                else{
                    listNotes!!.add(note)
                    noteAdapter!!.notifyDataSetChanged()
                }
            }
        }
    }
}
