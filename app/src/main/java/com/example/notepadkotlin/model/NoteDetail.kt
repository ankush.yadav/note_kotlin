package com.example.notepadkotlin.model

import android.os.Parcel
import android.os.Parcelable

data class NoteDetail(
    var id: Int,
    var tv_note_contain: String, var createdDate: String,
    var modifiedDate: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!
    )

    constructor() : this(0, "", "", "")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(tv_note_contain)
        parcel.writeString(createdDate)
        parcel.writeString(modifiedDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NoteDetail> {
        override fun createFromParcel(parcel: Parcel): NoteDetail {
            return NoteDetail(parcel)
        }

        override fun newArray(size: Int): Array<NoteDetail?> {
            return arrayOfNulls(size)
        }
    }
}
