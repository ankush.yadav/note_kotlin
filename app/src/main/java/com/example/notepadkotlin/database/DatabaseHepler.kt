package com.example.notepadkotlin.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import com.example.notepadkotlin.model.NoteDetail
import com.example.notepadkotlin.util.ConstantUtil.NoteTable
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


public class DatabaseHepler(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    private val context: Context = context

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "NoteDatabase"
    }

    override fun onCreate(db: SQLiteDatabase?) = try {
        val execSQL = db?.execSQL(
            """CREATE TABLE IF NOT EXISTS notes(
            ${NoteTable._ID} INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            ${NoteTable.Text} TEXT, 
            ${NoteTable.CREATED_AT} INTEGER,
            ${NoteTable.MODIFIED_AT} INTEGER)"""
        )
    } catch (e: SQLiteException) {
        try {
            throw IOException(e)
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS ${NoteTable._TABLE_NAME}")
        onCreate(db)
    }

    fun insert(taskName: String, createdDate: Calendar): Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(NoteTable.Text, taskName)
        contentValues.put(NoteTable.MODIFIED_AT, createdDate.timeInMillis)
        contentValues.put(NoteTable.CREATED_AT, createdDate.timeInMillis)
        db.replace(NoteTable._TABLE_NAME, null, contentValues)

        Toast.makeText(context, "Data Inserted Successfully", Toast.LENGTH_LONG).show()
        return true
    }

    fun update(taskName: String, dueDate: Calendar, noteId: Int): Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(NoteTable.Text, taskName)
        contentValues.put(NoteTable.MODIFIED_AT, dueDate.timeInMillis)
        db.update(NoteTable._TABLE_NAME, contentValues, "${NoteTable._ID}=$noteId", null)
        return true
    }


    fun delete(taskId: Int): Boolean {
        val db = this.writableDatabase
        db.delete(NoteTable._TABLE_NAME, "${NoteTable._ID}=$taskId", null)
        return true
    }

    fun fetchAllNote(): Cursor {
        val db = this.readableDatabase
        return db.rawQuery("SELECT * FROM ${NoteTable._TABLE_NAME}", null)
    }

    fun getAllNotes(): ArrayList<NoteDetail> {
        val cursor = fetchAllNote()
        val listNotes = ArrayList<NoteDetail>()
        if (cursor != null && cursor!!.getCount() > 0) {
            while (cursor!!.moveToNext()) {
                val note = NoteDetail()
                note.id = cursor!!.getInt(0)
                note.tv_note_contain = cursor!!.getString(1)
                note.createdDate = cursor!!.getString(2)
                note.modifiedDate = cursor!!.getString(3)
                println("Note --> $note")
                listNotes.add(note)
            }
        }
        return listNotes
    }
}

