package com.example.notepadkotlin.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.notepadkotlin.R
import android.view.LayoutInflater
import com.example.notepadkotlin.model.NoteDetail
import kotlinx.android.synthetic.main.row_note.view.*


class NoteAdapter(listNotes: ArrayList<NoteDetail>) :
    RecyclerView.Adapter<NoteAdapter.ViewHolder>() {

    private var listNotes: List<NoteDetail> = listNotes

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_note, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listNotes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val noteDetail: NoteDetail = listNotes[position]
        holder.tv_note_contain.text = noteDetail.tv_note_contain
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tv_note_contain = itemView.tv_note_contain!!
    }
}