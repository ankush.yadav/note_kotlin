package com.example.notepadkotlin.util

import com.example.notepadkotlin.util.ConstantUtil.NoteTable.CREATED_AT
import com.example.notepadkotlin.util.ConstantUtil.NoteTable.MODIFIED_AT
import com.example.notepadkotlin.util.ConstantUtil.NoteTable._ID
import com.example.notepadkotlin.util.ConstantUtil.NoteTable._TABLE_NAME
import com.example.notepadkotlin.util.ConstantUtil.NoteTable.Text

class ConstantUtil {
    object NoteTable {
        val _ID = "_id"
        val _TABLE_NAME = "notes"
        const val Text = "text"
        val CREATED_AT = "created_at"
        val MODIFIED_AT = "modified_at"
    }

    val EXTRA_TASK = "Task"

    var SQL_CREATE_ENTRIES = """CREATE TABLE $_TABLE_NAME (
                $_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                $Text TEXT, 
                $CREATED_AT INTEGER,
                $MODIFIED_AT INTEGER)"""

    var SQL_DELETE_ENTRY = "DROP TABLE IF EXISTS $_TABLE_NAME"

}
