package com.example.notepadkotlin

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.notepadkotlin.database.DatabaseHepler
import kotlinx.android.synthetic.main.activity_add_edit_note.*
import com.example.notepadkotlin.model.NoteDetail
import com.example.notepadkotlin.util.ConstantUtil
import java.util.*
import android.content.Intent
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T


class AddEditNoteActivity : AppCompatActivity() {

    private var isRequestUpdate: Boolean? = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit_note)
        var note: NoteDetail? = intent.getParcelableExtra(ConstantUtil().EXTRA_TASK)
        if (note != null) {
            isRequestUpdate = true
            edt_note_text.setText(note.tv_note_contain)
            btn_submit.text = "Update Note"
        } else
            note = NoteDetail()
        btn_submit.setOnClickListener(View.OnClickListener { v ->
            val currentTime: String = Calendar.getInstance().timeInMillis.toString()
            note.tv_note_contain = edt_note_text.text.toString()
            note.modifiedDate = currentTime
            val intent = Intent()
            if (isRequestUpdate!!) {
                intent.putExtra("Task", note)
                intent.putExtra("Modified", true)
                setResult(Activity.RESULT_OK, intent)
                DatabaseHepler(this).update(
                    edt_note_text.text.toString(),
                    Calendar.getInstance(),
                    note!!.id
                )
            } else {
                DatabaseHepler(this).insert(edt_note_text.text.toString(), Calendar.getInstance())
                note.createdDate = currentTime
                intent.putExtra("Task", note)
                intent.putExtra("Modified", false)
                setResult(Activity.RESULT_OK, intent)
            }
            finish()
        })
    }
}
